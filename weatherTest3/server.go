package main

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo/v4"
	"io/ioutil"
	"net/http"
)

type Weather struct {

	Weather []struct {
		ID          int    `json:"id"`
		Main        string `json:"main"`
		Description string `json:"description"`
		Icon        string `json:"icon"`
	} `json:"weather"`

	Main struct {
		Temp      float64 `json:"temp"`
		FeelsLike float64 `json:"feels_like"`
		Pressure  int     `json:"pressure"`
		Humidity  int     `json:"humidity"`
	} `json:"main"`
	Visibility int `json:"visibility"`
	Wind       struct {
		Speed float64 `json:"speed"`
		Deg   int     `json:"deg"`
	} `json:"wind"`


	Sys struct {
		Type    int    `json:"type"`
		ID      int    `json:"id"`
		Country string `json:"country"`
		Sunrise int    `json:"sunrise"`
		Sunset  int    `json:"sunset"`
	} `json:"sys"`

}

func main(){
	weatherDetails()

}


func weatherDetails() {
	e := echo.New()
	e.GET("/", func(c echo.Context) error {
		//var json string
		city := c.FormValue("city_name")
		var weather Weather

		url := "https://api.openweathermap.org/data/2.5/weather?q="
		value :=url + city+ "&appid=32ffb0f00286581e0c3317b51c416b86"
		fmt.Println(value)

		res,err:=http.Get(value)

		fmt.Println(res)
		if err != nil {
			return c.JSON(http.StatusCreated, err)
		}
		defer res.Body.Close()
		body, err:= ioutil.ReadAll(res.Body)
		if err != nil {
			return c.JSON(http.StatusCreated, err)
		}
		err = json.Unmarshal(body, &weather)
		if err != nil {
			return c.JSON(http.StatusCreated, err)
		}
		fmt.Println(weather)
		return c.JSON(http.StatusCreated, weather)
	})

	e.Logger.Fatal(e.Start(":2000"))

}

